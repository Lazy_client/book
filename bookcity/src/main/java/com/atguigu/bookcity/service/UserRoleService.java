package com.atguigu.bookcity.service;

import com.atguigu.bookcity.entity.UserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zsc
 * @since 2021-05-21
 */
public interface UserRoleService extends IService<UserRole> {

}
