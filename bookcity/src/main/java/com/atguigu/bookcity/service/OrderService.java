package com.atguigu.bookcity.service;

import com.atguigu.bookcity.entity.OrderForm;
import com.atguigu.bookcity.entity.vo.orderVo;
import com.atguigu.bookcity.vo.CommonResult;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hxx
 * @since 2021-03-20
 */
public interface OrderService extends IService<OrderForm> {
    HashMap<String, Object> getOrderMsg(String userId);

    Map<String,Object> createOrder(List<orderVo> orderVoList, String userId);

    CommonResult getRank();

    void deleteOrder(List<String> orderIds);

//    void createOrder(List<orderVo> orderVoList);
}
