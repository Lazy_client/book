package com.atguigu.bookcity.service;

public interface EmailService {
    void getEmailCodeForRegister(String email);
    void sendEmail(String email, String what,String content);
}
