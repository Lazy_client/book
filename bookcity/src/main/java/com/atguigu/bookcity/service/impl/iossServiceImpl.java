package com.atguigu.bookcity.service.impl;

import cn.hutool.core.util.IdUtil;
import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.atguigu.bookcity.exception.BookCityException;
import com.atguigu.bookcity.service.iossService;
import com.atguigu.bookcity.utils.constantPropertiesUtils;
import org.joda.time.DateTime;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author hxx
 * @date 2021/3/30 23:58
 */
@Service
public class iossServiceImpl implements iossService {

    @Override
    public String uploadFileAvatar(MultipartFile file) {
        // Endpoint以杭州为例，其它Region请按实际情况填写。
        String endpoint = constantPropertiesUtils.END_POIND;
        // 阿里云主账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM账号进行API访问或日常运维，请登录RAM控制台创建RAM账号。
        String accessKeyId = constantPropertiesUtils.ACCESS_KEY_ID;
        String accessKeySecret = constantPropertiesUtils.ACCESS_KEY_SECRET;
        String bucketName = constantPropertiesUtils.BUCKET_NAME;
        // <yourObjectName>上传文件到OSS时需要指定包含文件后缀在内的完整路径，例如abc/efg/123.jpg。
        OSS ossClient;
        try {
            String datetime = new DateTime().toString("yyyy/MM/dd");
            String simpleUUID = IdUtil.simpleUUID();
            String fileName = datetime+"/"+simpleUUID+file.getOriginalFilename();

            // 创建OSSClient实例。
            ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);

            // 上传文件到指定的存储空间（bucketName）并将其保存为指定的文件名称（objectName）。
            ossClient.putObject(bucketName, fileName,file.getInputStream());
            ossClient.shutdown();
            return "https://"+bucketName+"."+endpoint+"/"+fileName;
        }catch (Exception e){
            e.printStackTrace();
            throw new BookCityException(201,"上传失败");
        }
    }
}
