package com.atguigu.bookcity.service.impl;

import com.alibaba.excel.EasyExcel;
import com.atguigu.bookcity.entity.excel.BookExcelListener;
import com.atguigu.bookcity.entity.excel.ExcelBook;
import com.atguigu.bookcity.service.BookService;
import com.atguigu.bookcity.service.ExcelService;
import com.atguigu.bookcity.vo.CommonResult;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;

@Service
@Transactional
public class ExcelServiceImpl implements ExcelService {
    @Override
    public CommonResult uploadBooks(MultipartFile file, BookService bookService) {
        InputStream inputStream;
        try {
            inputStream = file.getInputStream();
            EasyExcel.read(inputStream, ExcelBook.class, new BookExcelListener(bookService))
                    .sheet()
                    .doRead();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return CommonResult.ok();
    }
}
