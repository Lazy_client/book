package com.atguigu.bookcity.service;

import com.atguigu.bookcity.entity.User;
import com.atguigu.bookcity.entity.vo.RegisterVo;
import com.atguigu.bookcity.entity.vo.UserVo;
import com.atguigu.bookcity.vo.CommonResult;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hxx
 * @since 2021-03-20
 */
public interface UserService extends IService<User> {

    CommonResult register(RegisterVo vo);

    CommonResult login(String name, String password);

    CommonResult adminRegister(RegisterVo vo, String role);

    void modifyUserRoleById(String userId, String newRole);

    boolean modifyPassword(UserVo vo);

    void resetPassword(String username);
}
