package com.atguigu.bookcity.service;

import com.atguigu.bookcity.vo.CommonResult;
import org.springframework.web.multipart.MultipartFile;

import java.math.BigDecimal;

public interface ActivitiService {
    CommonResult submitBookPurchase(MultipartFile file, BigDecimal amount);
}
