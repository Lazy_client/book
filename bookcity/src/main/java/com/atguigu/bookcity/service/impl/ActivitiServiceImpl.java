package com.atguigu.bookcity.service.impl;

import com.atguigu.bookcity.entity.vo.BookSubmitVo;
import com.atguigu.bookcity.exception.BookCityException;
import com.atguigu.bookcity.service.ActivitiService;
import com.atguigu.bookcity.utils.RedisUtil;
import com.atguigu.bookcity.vo.CommonResult;
import com.jvm123.excs.service.ExImporter;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.List;

@Service
public class ActivitiServiceImpl implements ActivitiService {
    @Resource
    private ExImporter exImporter;

    @Resource
    private RedisUtil redisUtil;

    @Override
    public CommonResult submitBookPurchase(MultipartFile file, BigDecimal amount) {
        InputStream inputStream;
        List<BookSubmitVo> voList;
        try {
            inputStream = file.getInputStream();
            voList = exImporter.convert(inputStream, BookSubmitVo.class);
        } catch (IOException e) {
            e.printStackTrace();
            throw new BookCityException(500, "Excel上传失败");
        }
        //  todo
        //前台上传的Excel 已转为List<BookSubmitVo> voList


        try {
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return
                //  todo
                //  worker申请结束后,和前台交互返回数据

                CommonResult.ok().data("bookList", voList);
    }
}
