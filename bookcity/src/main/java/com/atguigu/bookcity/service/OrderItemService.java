package com.atguigu.bookcity.service;

import com.atguigu.bookcity.entity.OrderItem;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author hxx
 * @date 2021/3/26 23:03
 */
public interface OrderItemService extends IService<OrderItem> {
}
