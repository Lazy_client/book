package com.atguigu.bookcity.service.impl;

import com.atguigu.bookcity.entity.UserRole;
import com.atguigu.bookcity.mapper.UserRoleMapper;
import com.atguigu.bookcity.service.UserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zsc
 * @since 2021-05-21
 */
@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole> implements UserRoleService {

}
