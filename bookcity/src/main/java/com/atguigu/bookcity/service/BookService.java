package com.atguigu.bookcity.service;

import com.atguigu.bookcity.entity.Book;
import com.atguigu.bookcity.entity.vo.bookshow;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zsc
 * @since 2021-03-20
 */
public interface BookService extends IService<Book> {

    String addbook(Book book);



    List<bookshow> getAllBooks();

    bookshow getBookById(String bookId);
}
