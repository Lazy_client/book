package com.atguigu.bookcity.service;

import com.atguigu.bookcity.entity.BookPurchaseApply;
import com.atguigu.bookcity.entity.vo.BookPurchaseVo;
import com.atguigu.bookcity.utils.PageUtils;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zsc
 * @since 2021-05-31
 */

/**
 * @author ws
 * @since 2021-05-31
 */


public interface BookPurchaseApplyService extends IService<BookPurchaseApply> {

    //
    PageUtils queryPage(Map<String, Object> params);
    //BookPurchaseApply是实体类
    List<BookPurchaseApply> getMyTasks(String username, Integer page, Integer limit);

    boolean addApply(BookPurchaseVo bookPurchaseVo);

    List<BookPurchaseApply> getCandidateTasks(String memberId, Integer page, Integer limit);
    //通过传递申请人id，对应applicant_id，查询到对应的申请
    List<BookPurchaseApply> getByUserId(String userId);
}
