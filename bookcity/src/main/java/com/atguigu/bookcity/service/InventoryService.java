package com.atguigu.bookcity.service;

import com.atguigu.bookcity.entity.Inventory;
import com.atguigu.bookcity.entity.vo.InventoryVo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zsc
 * @since 2021-06-05
 */
public interface InventoryService extends IService<Inventory> {

    List<InventoryVo> listByBookIds(List<String> ids);

    List<InventoryVo> selectInventories();

    List<InventoryVo> listByBookNames(List<String> names);

    List<InventoryVo> getInventoriesLikeName(String name);

    List<InventoryVo> getNotEnoughInventories(Integer from,Integer to);

    void addInventories(List<Inventory> inventories);

    void removeInventoriesNotInBooks();
}
