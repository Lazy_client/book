package com.atguigu.bookcity.service.impl;

import com.atguigu.bookcity.exception.BookCityException;
import com.atguigu.bookcity.entity.Category;
import com.atguigu.bookcity.entity.vo.allcategory;
import com.atguigu.bookcity.entity.vo.categoryResult;
import com.atguigu.bookcity.mapper.CategoryMapper;
import com.atguigu.bookcity.service.CategoryService;
import com.atguigu.bookcity.utils.RedisUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author zsc
 * @since 2021-03-20
 */
@Service
@Transactional
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, Category> implements CategoryService {
    @Override
    public void updatefirstCategory(Category vo) {
        updateById(vo);
    }

    @Resource
    private RedisUtil redisUtil;

    /**
     * 添加一级分类
     *
     * @param category 一级分类对象
     * @return 返回唯一的id
     */
    @Override
    public String addfirstCategory(Category category) {
        category.setParentId("0");
        baseMapper.insert(category);
        return category.getId();
    }

    @Override
    public void deletefirstCategory(String id) {
        baseMapper.delete(new QueryWrapper<Category>().eq("parent_id", id));
        baseMapper.deleteById(id);
    }

    @Override
    public String addsecondCategory(Category category) {

        if (category.getDescription() != null || category.getTitle() != null || category.getParentId() != null) {
            baseMapper.insert(category);
            return category.getId();
        } else
            throw new BookCityException(201, "添加失败");
    }

    @Override
    public void deletesecondCategory(String id) {
        if (id != null)
            baseMapper.deleteById(id);
        else throw new BookCityException(201, "删除失败");
    }

    @Override
    public void updatesecondCategory(Category category) {
        baseMapper.updateById(category);
    }

    @Override
    public List<allcategory> getAllCategory() {
        List<allcategory> allcategories = (List<allcategory>) redisUtil.get("allcategories");

        if (allcategories == null) {
            QueryWrapper<Category> categoryQueryWrapper = new QueryWrapper<>();
            QueryWrapper<Category> queryWrapper = categoryQueryWrapper.eq("parent_id", 0);
            List<Category> list1 = list(queryWrapper);
            allcategories = new ArrayList<>();
            for (Category category : list1) {
                String id = category.getId();
                QueryWrapper<Category> queryWrapper1 = new QueryWrapper<>();
                queryWrapper1.eq("parent_id", id);
                List<Category> secondList = list(queryWrapper1);
                List<categoryResult> categoryResultList = new ArrayList<>();
                for (Category category1 : secondList) {
                    categoryResult categoryResult = new categoryResult();
                    BeanUtils.copyProperties(category1, categoryResult);
                    categoryResult.setLevel(1);
                    categoryResultList.add(categoryResult);
                }
                allcategory allcategory = new allcategory();
                BeanUtils.copyProperties(category, allcategory);
                allcategory.setLevel(0);
                allcategory.setChildren(categoryResultList);
                allcategories.add(allcategory);

            }
            redisUtil.set("allcategories", allcategories);
        }
        return allcategories;
    }
}
