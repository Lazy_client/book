package com.atguigu.bookcity.service;

import com.atguigu.bookcity.entity.Role;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zsc
 * @since 2021-06-01
 */
public interface RoleService extends IService<Role> {

}
