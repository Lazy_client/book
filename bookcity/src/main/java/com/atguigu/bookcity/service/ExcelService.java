package com.atguigu.bookcity.service;

import com.atguigu.bookcity.vo.CommonResult;
import org.springframework.web.multipart.MultipartFile;

public interface ExcelService {

    CommonResult uploadBooks(MultipartFile file, BookService bookService);
}
