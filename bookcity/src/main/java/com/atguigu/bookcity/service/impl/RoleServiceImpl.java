package com.atguigu.bookcity.service.impl;

import com.atguigu.bookcity.entity.Role;
import com.atguigu.bookcity.mapper.RoleMapper;
import com.atguigu.bookcity.service.RoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zsc
 * @since 2021-06-01
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {

}
