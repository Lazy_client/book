package com.atguigu.bookcity.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * @author hxx
 * @date 2021/3/30 23:57
 */
public interface iossService {
    String uploadFileAvatar(MultipartFile file);
}
