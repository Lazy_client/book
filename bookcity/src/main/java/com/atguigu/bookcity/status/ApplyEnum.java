package com.atguigu.bookcity.status;

public enum ApplyEnum {
    ASSIGNEE("assignee"),
    MANAGERS("manager"),
    FINANCIALS("financial"),
    APPLY_ID("applyId"),
    BOOKNUMBER("bookNumber"),
    PROCESSDEFINITIONKEY("bookPurchaseApply"),

    PENDING("PENDING"),
    APPLY_SUCCESS("APPLY_SUCCESS"),
    APPLY_FAILED("APPLY_FAILED");

    private String message;

    ApplyEnum(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }
}