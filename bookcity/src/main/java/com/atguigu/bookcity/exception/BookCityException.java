package com.atguigu.bookcity.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author hxx
 * @date 2021/2/19 19:50
 */
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BookCityException extends RuntimeException{
    private Integer code;
    private String msg;
}
