package com.atguigu.bookcity.utils;

import org.apache.shiro.crypto.hash.Md5Hash;

public class Md5 {
    public static String encryption(String password, String salt, int hashIterations) {
        return new Md5Hash(password, salt, hashIterations).toHex();
    }
}
