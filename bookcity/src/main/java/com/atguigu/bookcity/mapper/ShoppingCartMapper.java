package com.atguigu.bookcity.mapper;

import com.atguigu.bookcity.entity.ShoppingCart;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hxx
 * @since 2021-03-20
 */
@Mapper
public interface ShoppingCartMapper extends BaseMapper<ShoppingCart> {

    List<ShoppingCart> getAllCart(String userId);

    void removeCartsByIds(List<String> bookIds, String id);
}
