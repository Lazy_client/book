package com.atguigu.bookcity.mapper;

import com.atguigu.bookcity.entity.User;
import com.atguigu.bookcity.entity.vo.UserInfoVo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hxx
 * @since 2021-03-20
 */
@Mapper
@Repository
public interface UserMapper extends BaseMapper<User> {

   List<UserInfoVo> getUserInfo();

    UserInfoVo getUserInfoById(String userId);
}
