package com.atguigu.bookcity.mapper;

import com.atguigu.bookcity.entity.Inventory;
import com.atguigu.bookcity.entity.vo.InventoryVo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zsc
 * @since 2021-06-05
 */
@Mapper
public interface InventoryMapper extends BaseMapper<Inventory> {

    List<InventoryVo> listByBookIds(List<String> ids);

    List<InventoryVo> selectInventories();

    List<InventoryVo> listByBookNames(List<String> names);

    List<InventoryVo> getInventoriesLikeName(String name);

    List<InventoryVo> getNotEnoughInventories(Integer from,Integer to);

    void removeInventoriesNotInBooks();
}
