package com.atguigu.bookcity.mapper;

import com.atguigu.bookcity.entity.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zsc
 * @since 2021-06-01
 */
@Mapper
public interface RoleMapper extends BaseMapper<Role> {
    List<String> getRoleByIds(List<Integer> roleIds);

    String getRoleIdByName(String name);
}
