package com.atguigu.bookcity.mapper;

import com.atguigu.bookcity.entity.BookPurchaseApply;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zsc
 * @since 2021-05-31
 */

@Mapper
public interface BookPurchaseApplyMapper extends BaseMapper<BookPurchaseApply> {


}
