package com.atguigu.bookcity.mapper;

import com.atguigu.bookcity.entity.UserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zsc
 * @since 2021-05-21
 */
@Mapper
public interface UserRoleMapper extends BaseMapper<UserRole> {

    String getRole(String userId);

    String getMemberIdsByRoleId(String roleId);

    List<String> getRoles();

    String getRoleIdByName(String name);
}
