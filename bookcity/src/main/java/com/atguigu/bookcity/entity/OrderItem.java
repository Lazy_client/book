package com.atguigu.bookcity.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author hxx
 * @date 2021/3/26 22:24
 */
@Data
@TableName("order_item")
public class OrderItem {
    private static final long serialVersionUID = 1L;

    private String bookId;

    private Integer bookNumber;

    @TableId
    private String orderId;

    @TableField(exist = false)
    private Date gmtCreate;
    @TableField(exist = false)
    private BigDecimal price;
    @TableField(exist = false)
    private String bookName;
    @TableField(exist = false)
    private String cover;
    @TableField(exist = false)
    private String status;
    @TableField(exist = false)
    private BigDecimal total;

}
