package com.atguigu.bookcity.entity.vo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * @author hxx
 * @date 2021/3/20 12:30
 */
@Data
@ToString
@EqualsAndHashCode
public class orderVo {
    private String bookId;
    private Integer bookNumber;
}
