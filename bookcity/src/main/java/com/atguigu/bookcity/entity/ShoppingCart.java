package com.atguigu.bookcity.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author hxx
 * @since 2021-03-20
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "ShoppingCart对象")
public class ShoppingCart implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "book_id", type = IdType.ASSIGN_ID)
    private String bookId;
    private String userId;
    private Integer bookNumber;
    @TableField(exist = false)
    private String bookName;
    @TableField(exist = false)
    private String cover;
    @TableField(exist = false)
    private BigDecimal price;
    @TableField(exist = false)
    private BigDecimal total;

}
