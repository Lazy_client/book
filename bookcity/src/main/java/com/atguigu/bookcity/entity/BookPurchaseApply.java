package com.atguigu.bookcity.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author zsc
 * @since 2021-05-31
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="BookPurchaseApply对象", description="")
public class BookPurchaseApply implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "apply_id", type = IdType.ASSIGN_ID)
    private String applyId;

    private String bookId;

    private String bookName;
    //新增数量
    private Integer stock;

    private String applicantId;
    //申请人姓名
    private String applicantName;

    private String status;

    private Date applyStartTime;

    private Date applyEndTime;


}
