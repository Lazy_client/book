package com.atguigu.bookcity.entity.vo;

import lombok.Data;

/**
 * @author hxx
 * @date 2021/3/27 19:52
 */
@Data
public class categoryResult {
    private String id;
    private String title;
    private String description;
    private int level;
    private String parentId;
}
