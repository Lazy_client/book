package com.atguigu.bookcity.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author zsc
 * @since 2021-06-05
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="Inventory对象")
public class Inventory implements Serializable {

    private static final long serialVersionUID = 1L;

      @TableId(value = "book_id", type = IdType.ASSIGN_ID)
    private String bookId;

    private Integer number;


}
