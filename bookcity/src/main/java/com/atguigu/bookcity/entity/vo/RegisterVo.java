package com.atguigu.bookcity.entity.vo;

import lombok.Data;

@Data
public class RegisterVo {
    private String username;
    private String password;
    private String email;
    private String code;
}
