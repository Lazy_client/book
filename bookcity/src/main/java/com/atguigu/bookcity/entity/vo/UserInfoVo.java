package com.atguigu.bookcity.entity.vo;

import lombok.Data;

@Data
public class UserInfoVo {
    private String id;
    private String username;
    private String roleName;
}
