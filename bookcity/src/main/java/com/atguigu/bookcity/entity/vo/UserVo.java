package com.atguigu.bookcity.entity.vo;

import lombok.Data;

@Data
public class UserVo {
    private String username;
    private String primaryPassword;
    private String password;
}
