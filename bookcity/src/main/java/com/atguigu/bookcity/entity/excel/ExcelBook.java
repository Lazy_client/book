package com.atguigu.bookcity.entity.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class ExcelBook {
    @ExcelProperty(value = "书名",index = 0)
    private String bookName;

    @ExcelProperty(value = "封面",index = 1)
    private String cover;

    @ExcelProperty(value = "价格",index = 2)
    private BigDecimal price;

    @ExcelProperty(value = "作者",index = 3)
    private String author;

    @ExcelProperty(value = "出版社",index = 4)
    private String press;

    @ExcelProperty(value = "出版时间",index = 5)
    @DateTimeFormat("yyyy-MM-dd HH:mm:ss")
    private Date publishingTime;

    @ExcelProperty(value = "印刷时间",index = 6)
    @DateTimeFormat("yyyy-MM-dd HH:mm:ss")
    private Date printingTime;

    @ExcelProperty(value = "一级分类",index = 7)
    private String firstId;

    @ExcelProperty(value = "二级分类",index = 8)
    private String secondId;
}
