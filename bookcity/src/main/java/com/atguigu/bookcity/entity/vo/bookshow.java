package com.atguigu.bookcity.entity.vo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author hxx
 * @date 2021/3/31 20:02
 */
@Data
public class bookshow {
    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    private String bookName;

    private String cover;

    private BigDecimal price;

    private String author;

    private String press;

    private Date publishingTime;

    private Date printingTime;

    private String firstId;

    private String firstcategory;

    private String secondId;

    private String secondcategory;

    @TableField(fill = FieldFill.INSERT)
    private Date gmtCreate;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date gmtModified;
}
