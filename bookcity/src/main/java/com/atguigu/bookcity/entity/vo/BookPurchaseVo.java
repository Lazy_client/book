package com.atguigu.bookcity.entity.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;

/**
 * @Author ws
 * @create 2021/5/26 20:22
 */
@Data
@ApiModel
public class BookPurchaseVo implements Serializable {

    private static final long serialVersionUID = 1L;
    //申请id
    private String applyId;
    //书目id
    private String bookId;
    //入库数量
    private Integer stock;
    //申请者id
    private String applicantId;
    //申请者姓名
    private String applicantName;
}
