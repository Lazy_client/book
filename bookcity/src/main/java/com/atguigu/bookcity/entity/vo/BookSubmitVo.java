package com.atguigu.bookcity.entity.vo;

import lombok.Data;

@Data
public class BookSubmitVo  {
    private String bookName;
    private Double price;
    private Integer bookNumber;
}
