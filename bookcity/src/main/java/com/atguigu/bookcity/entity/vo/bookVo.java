package com.atguigu.bookcity.entity.vo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author hxx
 * @date 2021/3/30 18:42
 */
@Data
@ToString
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="Book前端的对象")
public class bookVo implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    private String bookName;

    private String cover;

    private BigDecimal price;

    private String author;

    private String press;

    private Date publishingTime;

    private Date printingTime;

    private String[] categories;

    @TableField(fill = FieldFill.INSERT)
    private Date gmtCreate;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date gmtModified;
}
