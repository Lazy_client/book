package com.atguigu.bookcity.entity.vo;

import lombok.Data;

import java.util.List;

/**
 * @author hxx
 * @date 2021/3/28 16:31
 */
@Data
public class allcategory {
    private String id;
    private String title;
    private String description;
    private int level;
    private int parentId;
    private List<categoryResult> children;
}
