package com.atguigu.bookcity.entity.vo;

import lombok.Data;

@Data
public class InventoryVo {
    String id;
    Integer number;
    String bookName;
}
