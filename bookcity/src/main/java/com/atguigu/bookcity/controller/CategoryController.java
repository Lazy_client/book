package com.atguigu.bookcity.controller;


import com.atguigu.bookcity.entity.Category;
import com.atguigu.bookcity.entity.vo.allcategory;
import com.atguigu.bookcity.service.CategoryService;
import com.atguigu.bookcity.utils.RedisUtil;
import com.atguigu.bookcity.vo.CommonResult;
import io.swagger.annotations.Api;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author zsc
 * @since 2021-03-19
 */
@CrossOrigin
@RestController
@Api(tags = {"分类管理"})
@RequestMapping("/bookcity/category")
public class CategoryController {

    @Resource
    CategoryService categoryService;

    @Resource
    private RedisUtil redisUtil;
    //添加一级分类
    @RequiresRoles("admin")
    @PostMapping("/addfirstCategory")
    public CommonResult addfirstCategory(@RequestBody Category category) {
        String id = categoryService.addfirstCategory(category);
        redisUtil.del("allcategories");
        return CommonResult.ok().data("id", id);
    }


    //修改一级分类
    @PostMapping("/updatefirstCategory")
    @RequiresRoles("admin")
    public CommonResult updateFirstCategory(@RequestBody Category category) {
        categoryService.updatefirstCategory(category);
        redisUtil.del("allcategories");
        return CommonResult.ok();
    }

    //删除一级分类
    @PostMapping("/deletefirstCategory/{id}")
    @RequiresRoles("admin")
    public CommonResult deleteFirstCategory(@PathVariable("id") String id) {
        categoryService.deletefirstCategory(id);
        redisUtil.del("allcategories");
        return CommonResult.ok();
    }

    //添加二级分类
    @PostMapping("/addsecondCategory")
    @RequiresRoles("admin")
    public CommonResult addSecondCategory(@RequestBody Category category) {

        String id = categoryService.addsecondCategory(category);
        redisUtil.del("allcategories");
        return CommonResult.ok().data("id", id);
    }

    //删除二级分类
    @PostMapping("/deletesecondCategory/{id}")
    @RequiresRoles("admin")
    public CommonResult deleteSecondCategory(@PathVariable String id) {
        categoryService.deletesecondCategory(id);
        redisUtil.del("allcategories");
        return CommonResult.ok();
    }

    //修改二级分类
    @PostMapping("/updatesecondCategory")
    @RequiresRoles("admin")
    public CommonResult updateSecondCategory(@RequestBody Category category) {
        categoryService.updatesecondCategory(category);
        redisUtil.del("allcategories");
        return CommonResult.ok();
    }

    @PostMapping("/getAllCategory")
    public CommonResult getAllCategory() {
        List<allcategory> allCategories =categoryService.getAllCategory();
        return CommonResult.ok().data("data", allCategories);
    }
}

