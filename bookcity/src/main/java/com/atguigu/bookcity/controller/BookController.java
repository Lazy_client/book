package com.atguigu.bookcity.controller;

import com.atguigu.bookcity.entity.Book;
import com.atguigu.bookcity.entity.vo.bookVo;
import com.atguigu.bookcity.entity.vo.bookshow;
import com.atguigu.bookcity.service.BookService;
import com.atguigu.bookcity.service.iossService;
import com.atguigu.bookcity.vo.CommonResult;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author zsc
 * @since 2021-03-19
 */
@CrossOrigin
@RestController
@Api(tags = {"图书管理"})
@RequestMapping("/bookcity/book")
public class BookController {
    @Resource
    private BookService bookService;

    @Resource
    private iossService iossService;

    //增加图书
    @PostMapping("/addbook")
    @RequiresRoles("admin")
    public CommonResult addBook(@RequestBody Book book) {
        String id = bookService.addbook(book);
        return CommonResult.ok().data("bookId", id);
    }

    //增加图书
    @PostMapping("/newaddbook")
    @RequiresRoles("admin")
    public CommonResult newaddbook(@RequestBody bookVo bookvo) {
        String[] categories = bookvo.getCategories();
        Book book = new Book();
        BeanUtils.copyProperties(bookvo, book);
        book.setFirstId(categories[0]);
        book.setSecondId(categories[1]);
        String id = bookService.addbook(book);
        return CommonResult.ok().data("bookId", id);
    }

    //分页模糊查询
    @PostMapping("/selectbycondition")
    public CommonResult selectByCondition(@RequestParam String condition,
                                          @RequestParam int startPage) {

        QueryWrapper<Book> wrapper = new QueryWrapper<>();
        wrapper.like("book_name", condition);
//        List<Book> books = bookService.list(wrapper);
        Page<Book> bookPage = new Page<>(startPage, 12);
        bookService.getBaseMapper()
                .selectPage(bookPage, wrapper);
        return CommonResult.ok().data("books", bookPage);
    }

    @GetMapping("/getbookbyCategory/{firstCategory}/{secondCategory}")
    public CommonResult getBookByCategory(@PathVariable("firstCategory") String firstCategory,
                                          @PathVariable("secondCategory") String secondCategory) {
        //图书表的条件查询
        QueryWrapper<Book> wrapper = new QueryWrapper<>();
        wrapper.eq("first_id", firstCategory)
                .eq("second_id", secondCategory);
        List<Book> books = bookService.list(wrapper);
        return CommonResult.ok().data("books", books);
    }

    //图书删除
    @GetMapping("/deletebook/{bookId}")
    @RequiresRoles("admin")
    public CommonResult deleteBook(@PathVariable("bookId") String bookId) {
        bookService.removeById(bookId);
        return CommonResult.ok();
    }

    //图书修改
    @PostMapping("/updatebook")
    @RequiresRoles("admin")
    public CommonResult updateBook(@RequestBody Book book) {
        bookService.updateById(book);
        return CommonResult.ok();
    }

    //分页查询
    @GetMapping("/getPage/{start}")
    public CommonResult getPage(@PathVariable("start") int startPage) {
        Page<Book> bookPage = new Page<>(startPage, 12);
        bookService.page(bookPage, null);
        List<Book> books = bookPage.getRecords();
        return CommonResult.ok().data("books", books);
    }

    @GetMapping("/getAllbooks")
    public CommonResult getAllBooks() {
        List<bookshow> bookShows = bookService.getAllBooks();
        return CommonResult.ok().data("books", bookShows);
    }

    @GetMapping("/getbookbyid/{id}")
    public CommonResult getBookById(@PathVariable("id") String bookId) {
        bookshow bookshow = bookService.getBookById(bookId);
        return CommonResult.ok().data(bookId, bookshow);
    }

    @ApiOperation(
            value = "图书封面上传",
            notes = "admin权限",
            produces = "application/json")
    @PostMapping("/upload")
    @RequiresRoles("admin")
    public CommonResult uploadFile(MultipartFile file) {
        String url = iossService.uploadFileAvatar(file);
        return CommonResult.ok().data("url", url);
    }

}

