package com.atguigu.bookcity.controller;


import com.atguigu.bookcity.entity.Inventory;
import com.atguigu.bookcity.entity.vo.InventoryVo;
import com.atguigu.bookcity.service.InventoryService;
import com.atguigu.bookcity.vo.CommonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author zsc
 * @since 2021-06-05
 */
@Api(tags = "库存服务")
@RestController
@CrossOrigin
@RequestMapping("/bookcity/inventory")
@RequiresRoles(value = {"manager", "admin", "financial", "worker"}, logical = Logical.OR)
public class InventoryController {

    @Resource
    private InventoryService inventoryService;

    @ApiOperation(
            value = "获取所有书的库存数据",
            notes = "后台角色权限使用此接口",
            produces = "application/json")
    @GetMapping("/getInventories")
    public CommonResult selectInventories() {
        List<InventoryVo> inventories = inventoryService.selectInventories();
        return CommonResult.ok().data("inventories", inventories);
    }

    @ApiOperation(
            value = "根据bookId获取书的库存数据",
            notes = "后台角色权限使用此接口",
            produces = "application/json")
    @GetMapping("/getInventoriesByIds")
    public CommonResult getInventoriesByIds(@RequestParam List<String> ids) {
        List<InventoryVo> inventories = inventoryService.listByBookIds(ids);
        return CommonResult.ok().data("inventories", inventories);
    }

    @ApiOperation(
            value = "根据book_name获取书的库存数据",
            notes = "后台角色权限使用此接口",
            produces = "application/json")
    @GetMapping("/getInventoriesByNames")
    public CommonResult getInventoriesByNames(@RequestParam List<String> names) {
        List<InventoryVo> inventories = inventoryService.listByBookNames(names);
        return CommonResult.ok().data("inventories", inventories);
    }

    @ApiOperation(
            value = "根据模糊书名获取书的库存数据",
            notes = "后台角色权限使用此接口",
            produces = "application/json")
    @GetMapping("/getInventoriesLikeName")
    public CommonResult getInventoriesLikeName(@RequestParam(required = false) String name) {
        List<InventoryVo> inventories = inventoryService.getInventoriesLikeName(name);
        return CommonResult.ok().data("inventories", inventories);
    }

    @ApiOperation(
            value = "增加库存",
            notes = "后台角色权限使用此接口",
            produces = "application/json")
    @PostMapping("/addInventories")
    public CommonResult addInventories(@RequestBody List<Inventory> inventories) {
        inventoryService.addInventories(inventories);
        return CommonResult.ok();
    }

    @ApiOperation(
            value = "按数量区间返回库存数据",
            notes = "后台角色权限使用此接口",
            produces = "application/json")
    @GetMapping("/getNotEnoughInventories")
    public CommonResult getNotEnoughInventories(@RequestParam(required = false) Integer from,
                                                @RequestParam Integer to) {
        List<InventoryVo> inventories = inventoryService.getNotEnoughInventories(from, to);
        return CommonResult.ok().data("inventories", inventories);
    }

    //DELETE FROM inventory WHERE book_id NOT IN (SELECT id FROM book)
    @GetMapping("/removeInventoriesNotInBooks")
    public CommonResult removeInventoriesNotInBooks() {
        inventoryService.removeInventoriesNotInBooks();
        return CommonResult.ok();
    }
}

