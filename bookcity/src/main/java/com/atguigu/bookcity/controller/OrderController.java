package com.atguigu.bookcity.controller;


import com.atguigu.bookcity.entity.OrderForm;
import com.atguigu.bookcity.entity.User;
import com.atguigu.bookcity.entity.vo.orderVo;
import com.atguigu.bookcity.service.OrderService;
import com.atguigu.bookcity.vo.CommonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.SecurityUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author hxx
 * @since 2021-03-19
 */
@CrossOrigin
@RestController
@Api(tags = {"订单管理"})
@RequestMapping("/bookcity/order")
//除了排名信息可以匿名访问，其他资源需要登录认证
public class OrderController {

    @Resource
    private OrderService orderService;

    @ApiOperation(value = "传递生成订单的商品的json形式的数组")
    @PostMapping("/createOrder")
    public CommonResult createOrder(@RequestBody List<orderVo> orderVoList) {
        User user = (User) SecurityUtils.getSubject().getPrincipal();
        String userId = user.getId();
        return CommonResult.ok()
                .data(orderService.createOrder(orderVoList, userId));
    }

    //点击付款后直接修改status
    @PostMapping("/updateStatus/{orderId}")
    public CommonResult updateStatus(@PathVariable("orderId") String orderId) {
        OrderForm order = new OrderForm();
        order.setId(orderId);
        order.setStatus(1);
        boolean b = orderService.updateById(order);
        if (b) {
            //修改status为1
            return CommonResult.ok();
        }
        return CommonResult.error();
    }

    //点击我的订单，查询订单信息
    @GetMapping("/getAllOrder")
    public CommonResult getAllOrder() {
        User user = (User) SecurityUtils.getSubject().getPrincipal();
        String userId = user.getId();
        HashMap<String, Object> resultMap = orderService.getOrderMsg(userId);
        return CommonResult.ok()
                .data("ok", resultMap.get("ok"))
                .data("error", resultMap.get("error"));
    }


    @GetMapping("/getRank")
    public CommonResult getRank() {
        return orderService.getRank();
    }

    @PostMapping("/deleteOrder")
    public CommonResult deleteOrder(@RequestParam List<String> orderIds) {
        orderService.deleteOrder(orderIds);
        return CommonResult.ok();
    }

}

