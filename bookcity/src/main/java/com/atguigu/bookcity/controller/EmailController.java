package com.atguigu.bookcity.controller;


import com.atguigu.bookcity.service.EmailService;
import com.atguigu.bookcity.vo.CommonResult;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@Api(tags = {"邮箱发送服务"})
@RequestMapping("/bookcity/email")
@CrossOrigin
public class EmailController {
   @Resource
   private EmailService emailservice;

    @GetMapping("/getEmailCode/{email}")
    public CommonResult getEmailCodeForRegist(@PathVariable String email) {
        emailservice.getEmailCodeForRegister(email);
        return CommonResult.ok();
    }
}
