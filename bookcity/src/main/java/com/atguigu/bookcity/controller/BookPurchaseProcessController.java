package com.atguigu.bookcity.controller;

import com.atguigu.bookcity.service.ActivitiService;
import com.atguigu.bookcity.vo.CommonResult;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.math.BigDecimal;

//@RestController
@SuppressWarnings("all")
@RequestMapping("/bookcity/purchase")
public class BookPurchaseProcessController {

    @Resource
    private ActivitiService activitiService;

    @ApiOperation(
            value = "员工提交购书申请",
            notes = "提交购书申请列表",
            produces = "application/json")
    @PostMapping("/submitBookPurchase")
//    @RequiresRoles("worker")
//    权限管理在测试阶段先放开，生产环境中放开
    public CommonResult submitBookPurchase(MultipartFile file,
                               @RequestParam BigDecimal amount) {
        return activitiService.submitBookPurchase(file, amount);
    }


    @ApiOperation(
            value = "审核购书申请",
            notes = "经理或财务对申请进行审核",
            produces = "application/json")
    @PostMapping("/audit")
//    @RequiresRoles(value = {"financial", "manager"}, logical = Logical.OR)
    public CommonResult audit(@RequestBody Boolean status) {
        return CommonResult.ok();
    }


}
