package com.atguigu.bookcity.vo;

/**
 * @author hxx
 * @date 2021/3/14 21:13
 */
public interface ResultCode {
    Integer SUCCESS=200;
    Integer ERROR=201;
}
