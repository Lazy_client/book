package com.atguigu.bookcity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;


@SpringBootApplication(exclude = SecurityAutoConfiguration.class)
public class BookcityApplicationMain {
    public static void main(String[] args) {
        SpringApplication.run(BookcityApplicationMain.class, args);
    }
}
