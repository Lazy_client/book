package com.atguigu.bookcity;

import com.atguigu.bookcity.entity.BookPurchaseApply;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest
public class DeploymentTest {
    @Resource
    private RepositoryService repositoryService;

    @Resource
    private RuntimeService runtimeService;

    @Test
    public void deploy() {
        //部署流程
        //1.加载processes里的资源
        //2.部署
        Deployment deployment = repositoryService.createDeployment()
                .name("bookPurchaseApply")
                .addClasspathResource("bpmn/bookPurchase.bpmn")
                .addClasspathResource("bpmn/bookPurchase.png")
                .deploy();

        System.out.println(deployment.getId());
        System.out.println(deployment.getName());
        System.out.println(deployment.getKey());
    }

    @Test
    public void listProcessDefine() {

        List<ProcessDefinition> bookPurchase = repositoryService.createProcessDefinitionQuery()
                .processDefinitionKey("bookPurchaseApply")
                .list();

        for (ProcessDefinition processDefinition : bookPurchase) {
            System.out.println(processDefinition.getId());
            System.out.println(processDefinition.getKey());
            System.out.println(processDefinition.getName());
        }
    }

    @Test
    public void startProcess() {
        Map<String, Object> variables = new HashMap<>();
        BookPurchaseApply bookPurchaseApply = new BookPurchaseApply();
        bookPurchaseApply.setApplicantId("1374383511008219137");
        variables.put("bookPurchaseApply", bookPurchaseApply);
        runtimeService.startProcessInstanceById("bookPurchaseApply:1:4bfd4eeb-c5b0-11eb-8fb2-64bc58b98cab", variables);

    }

    /**
     * 启动流程实例
     * `act_hi_actinst`    流程实例执行历史信息
     * `act_hi_identitylink` 流程参与用户的历史信息
     * `act_hi_procinst`     流程实例的历史信息
     * `act_hi_taskinst`     流程任务的历史信息
     * `act_ru_execution`    流程执行信息
     * `act_ru_identitylink`  流程的正在参与用户信息
     * `act_ru_task`         流程当前任务信息
     */
    @Test
    public void testStartProcess(){
//        1、创建ProcessEngine
//        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
////        2、获取RunTimeService
//        RuntimeService runtimeService = processEngine.getRuntimeService();
//        3、根据流程定义的id启动流程
        ProcessInstance instance = runtimeService.startProcessInstanceByKey("bookPurchase");
//        4、输出内容
        System.out.println("流程定义ID："+instance.getProcessDefinitionId());
        System.out.println("流程实例ID："+instance.getId());
        System.out.println("当前活动的ID："+instance.getActivityId());
    }
}
