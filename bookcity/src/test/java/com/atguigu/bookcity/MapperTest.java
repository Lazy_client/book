package com.atguigu.bookcity;

import com.atguigu.bookcity.mapper.InventoryMapper;
import com.atguigu.bookcity.mapper.ShoppingCartMapper;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
public class MapperTest {

    @Resource
    private InventoryMapper inventoryMapper;
    @Test
    public void mapperTest(){
        List<String> ids = new ArrayList<>();
        ids.add("1377816319133880322");
        ids.add("1377816322371883010");
        ids.add("1377824374298906625");
        System.out.println(inventoryMapper.listByBookIds(ids));
    }

    @Test
    public void testInventories(){
        System.out.println(inventoryMapper.selectInventories());
    }

    @Resource
    private ShoppingCartMapper cartMapper;

    @Test
    public void testNames(){
        cartMapper.getAllCart("1393930172965416962")
                .stream()
                .peek(cart -> cart.
                        setTotal(BigDecimal.valueOf(cart.getBookNumber()).multiply(cart.getPrice())))
                .forEach(System.out::println);
    }



}
