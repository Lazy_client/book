package com.atguigu.bookcity;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Test {
    @org.junit.Test
    public void test(){
        List<String> list = new ArrayList<>();
        list.add("11111");
        list.add("11111");
        list.add("11111");
        list.add("11111");
        list.add("11111");
        list.add("11111");
        list.add("11111");
        list.add("11111");
        list.add("22222");
        list.add("22222");
        list.add("22222");
        list.add("22222");
        list.add("22222");
        list.add("22222");
        list.add("33333");
        list.add("33333");
        list.add("33333");
        list.add("33333");
        list.add("33333");
        list.add("33333");
        list.add("33333");
        list.add("444444");
        System.out.println(list.remove("33333"));
        System.out.println(list);
    }

    private static List<String> toRepeat(List<String> list) {
        //定义一个Set集合
        Set<String> set = new HashSet<>();
        //新定义一个List集合
        List<String> newList = new ArrayList<>();
        //迭代遍历集合，利用Set集合的特性（不含有重复对象），即可达到去重的目的
        for (String element : list) {
            if (set.add(element)) {
                newList.add(element);
            }
        }
        return newList;
    }
}
