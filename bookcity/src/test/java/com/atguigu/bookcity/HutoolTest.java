package com.atguigu.bookcity;

import cn.hutool.core.util.RandomUtil;
import com.atguigu.bookcity.utils.Md5;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.junit.Test;

public class HutoolTest {
    @Test
    public void hutoolTest(){

        System.out.println(RandomUtil.randomString(32));

        String salt =new String(RandomUtil.randomBytes(12));

        String memberPassword = new Md5Hash("123",salt,1024).toHex();
        String password = new Md5Hash("123",salt,1024).toHex();
        System.out.println(password.equals(memberPassword));

        System.out.println(RandomUtil.randomString(20));
        System.out.println("c1ab330c98d44500a3a9ac6a182b3ff5accc1d7573340f7ae4bd875a47bb4f85".length());

        System.out.println("fc9680f715fb4aecae40670e9174f507".length());

        System.out.println(Md5.encryption("shiro","g71nri7hf10anlgp3yqj",1024));
    }
}
