package com.atguigu.bookcity;

//import com.itheima.demo.pojo.Evection;
import com.atguigu.bookcity.entity.BookPurchaseApply;
import org.activiti.engine.*;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * 测试流程变量
 */
@SpringBootTest
public class TestVariables {
    /**
     * 流程部署
     */
    @Resource
    private RepositoryService repositoryService;

    @Resource
    private RuntimeService runtimeService;

    @org.junit.jupiter.api.Test
    public void testDeployment(){
//        1、创建ProcessEngine
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
//        2、获取RepositoryServcie
        RepositoryService repositoryService = processEngine.getRepositoryService();
//        3、使用service进行流程的部署，定义一个流程的名字，把bpmn和png部署到数据中
        Deployment deploy = repositoryService.createDeployment()
                .name("购书申请流程")
                .addClasspathResource("bpmn/bookPurchaseApply.bpmn")
                .deploy();
//        4、输出部署信息
        System.out.println("流程部署id="+deploy.getId());
        System.out.println("流程部署名字="+deploy.getName());
    }

    /**
     * 启动流程 的时候设置流程变量
     * 设置流程变量num
     * 设置任务负责人
     */
    @org.junit.jupiter.api.Test
    public void testStartProcess(){
//        获取流程引擎
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
//        获取RunTimeService
        RuntimeService runtimeService = processEngine.getRuntimeService();
//        流程定义的Key
        String key = "bookPurchaseApply";
//        流程变量的map
        Map<String,Object> variables = new HashMap<>();
//        设置流程变量
//        Evection evection = new Evection();
//        设置出差日期
        //evection.setNum(2d);
        BookPurchaseApply bookPurchaseApply = new BookPurchaseApply();
        bookPurchaseApply.setApplicantName("ws");
        bookPurchaseApply.setStock(10001);
//        把流程变量的pojo放入map
        variables.put("bookPurchaseApply",bookPurchaseApply);
        variables.put("isPass", true);
//        设定任务的负责人
//        variables.put("assignee0","李四");
//        variables.put("assignee1","王经理");
//        variables.put("assignee2","杨总经理");
//        variables.put("assignee3","张财务");
//        启动流程
        runtimeService.startProcessInstanceByKey(key,variables);
    }

    /**
     * 完成个人任务
     */
    @org.junit.jupiter.api.Test
    public void completeTask(){
//        流程定义的Key
        String key = "bookPurchaseApply";
//        任务负责人
//        String assignee = "李四";

        String assignee = "ws";
        //        获取流程引擎
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
//        获取taskservice
        TaskService taskService = processEngine.getTaskService();
//        查询任务
        Task task = taskService.createTaskQuery()
                .processDefinitionKey(key)
                .taskAssignee(assignee)
                .singleResult();
        if(task != null){
            //     根据任务id来   完成任务
            taskService.complete(task.getId());
        }

    }
}
